import binarytree as bt
from binarytree import build

nodes = [1, 2, 2, 3, 4, 4, 3]
binary_tree = build(nodes)

print(f'Binary Tree from list: \n {binary_tree}')
print('Is tree balance?', binary_tree.is_balanced)
print('The height of the tree: ', binary_tree.height)
print('Is this tree symmetrical: ',binary_tree.is_symmetric)

node = [4, 2, 6, 1, 3, 5, 7]
binary_tree_a = build(node)
print('Is the tree a BST?', binary_tree_a.is_bst)
print('Is this binary tree strict?', binary_tree.is_strict)
print('The min heap is: ', binary_tree_a.min_node_value)
print('The max heap is: ', binary_tree_a.max_node_value)

nod = [1, 2, 3, 4, 5]
binary_tree_b = build(nod)
print('THe leaf count is: ', binary_tree_b.leaf_count)
print('The levels of the tree: ', binary_tree_b.levels)











